# DACI Helper

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)

Decision making with the right combination of robustness and speed is a challenge, but improving the process can have a dramatic impact. To facilitate better decision making, this Forge app can be inserted into any page generated/formatted in line with the [DACI template](https://marketplace.atlassian.com/apps/1216027/team-playbook-daci?hosting=cloud&tab=overview). The app introspects the page and looks for missing, inconsistent or incorrect information based on the [DACI framework](https://www.atlassian.com/team-playbook/plays/daci).

If your DACI is in line with the guidelines, then the app will present the following:

![Valid DACI](docs/images/daci-success.png)

If the app detects problems in the page, then is will present something like the following:

![Invalid DACI](docs/images/daci-fail.png)

## Installation

If this is your first time using Forge, the [getting started](https://developer.atlassian.com/platform/forge/set-up-forge/) guide will help you install the prerequisites.

If you already have a Forge environment setup, you can deploy this example straight away. Visit our [example apps](https://developer.atlassian.com/platform/forge/example-apps/) page for installation steps.


## Documentation

The app's [manifest.yml](./manifest.yml) contains two modules:

1. A [macro module](https://developer.atlassian.com/platform/forge/manifest-reference/#macro) that specifies the metadata displayed to the user in the Confluence editor's quick insert menu.
1. A corresponding [function module](https://developer.atlassian.com/platform/forge/manifest-reference/#function) that implements the macro logic.

Much of the app's logic comprises parsing the Confluence page that the macro is embedded in. Two classes, [DaciChecker](src/DaciChecker.ts) and [AdfUtil](src/AdfUtil.ts) implement this parsing. `AdfUtil` provides generic methods for parsing [ADF documents](https://developer.atlassian.com/platform/atlassian-document-format/) whilst `DaciChecker` is specific to DACIs. 

In addition to this, the [PageUtil](src/PageUtil.ts) class contains a method to fetch the ADF of the page that the app's macro is executing in.

The app's UI is implemented using these features:
 
 - [`Text`](https://developer.atlassian.com/platform/forge/ui-components/text) component.
 - [`Image`](https://developer.atlassian.com/platform/forge/ui-components/image) component.
 - [`useProductContext`](https://developer.atlassian.com/platform/forge/ui-hooks-reference/#useproductcontext) and [`useAction`](https://developer.atlassian.com/platform/forge/ui-hooks-reference/#useAction) hooks.

At the time of implementation, Forge UI capabilities were quite limited. In order to generate a suitably attractive summary status, the app generates a banner with a green/red theme depending on the results of the checks. This banner is implemented using SVG which is inserted into the page using an `&lt;img/&gt;` tag.


## Contributions

Contributions to DACI Helper are welcome! Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details. 

## License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.