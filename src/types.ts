
// export const statusNotStarted = ''

// export const allowedStatuses = ['not started', 'in progress', 'done', 'obsolete'];
// export const allowedImpacts = ['low', 'medium', 'high'];

export enum Status {
  NotStarted = 'not started',
  InProgress = 'in progress',
  Done = 'done',
  Obsolete = 'obsolete'
};

export const allowedStatuses = [Status.NotStarted, Status.InProgress, Status.Done, Status.Obsolete];

export enum Impact {
  Low = 'low',
  Medium = 'medium',
  High = 'high'
};

export const allowedImpacts = [Impact.Low, Impact.Medium, Impact.High];

export enum ValidationResultType {
  Pass = 'pass',
  Warning = 'warning',
  Error = 'error',
}

export type ValidationResult = {
  type: ValidationResultType,
  message: string
}

export type DaciValidation = {
  results: ValidationResult[],
  status?: string,
  impact?: string,
  driver?: string,
  informed?: string,
  contributors?: string,
  dueDate?: Date,
  overdue?: boolean,
  millisecondsUntilDue?: number,
  backgroundText?: string,
  backgroundLinkCount?: number,
  relevantDataText?: string,
  relevantDataLinkCount?: number
};

export enum ProConType {
  Minus = 'Minus',
  Plus = 'Plus',
  Warning = 'Warning',
  Question = 'Question'
}
