import pbc from './PBC';
import { Status } from './types';

class Util {

  isIn = (value: any, items: any[]): boolean => {
    pbc.assertTruthy(items, 'items');
    for (const item of items) {
      if (item === value) {
        return true;
      }
    }
    return false;
  };

  statusOf = (text: string) => {
    if (text) {
      const textLower = text.toLowerCase().trim();
      if (textLower === 'not started') {
        return Status.NotStarted;
      } else if (textLower === 'in progress') {
        return Status.InProgress;
      } else if (textLower === 'done') {
        return Status.Done;
      } else if (textLower === 'obsolete') {
        return Status.Obsolete;
      }
    }
    return undefined;
  };

}

export default new Util()
