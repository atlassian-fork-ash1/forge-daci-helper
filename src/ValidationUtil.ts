import {
  ValidationResultType,
  ValidationResult
} from './types';

class ValidationUtil {

  countPasses = (results: ValidationResult[]): number => {
    let count = 0;
    for (const result of results) {
      if (result.type === ValidationResultType.Pass) {
        count++;
      }
    }
    return count;
  };

  countProblems = (results: ValidationResult[]): number => {
    let count = 0;
    for (const result of results) {
      if (result.type === ValidationResultType.Error || result.type === ValidationResultType.Warning) {
        count++;
      }
    }
    return count;
  };

  countErrors = (results: ValidationResult[]): number => {
    let count = 0;
    for (const result of results) {
      if (result.type === ValidationResultType.Error) {
        count++;
      }
    }
    return count;
  };

  logResults = (results: ValidationResult[]): number => {
    let count = 0;
    for (const result of results) {
      console.log(` * ${result.type.toUpperCase()}: ${result.message}`);
    }
    return count;
  };

}

export default new ValidationUtil()
