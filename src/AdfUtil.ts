import pbc from './PBC';

export type AdfNode = {
  type: string,
  attrs?: any,
  content?: any[],
  marks?: any[],
  text?: string
}

export type AdfContext = {
  parentNodes: AdfNode[]
}

export type IterationContext = {
  level: number,
  continueIterating: boolean,
  parentNodes: AdfNode[]
}

export type TableCellPosition = {
  columnIndex: number,
  rowIndex: number
}

export type LocatedNode = {
  node: AdfNode,
  context: AdfContext
}

class AdfUtil {

  getParentLocation = (locatedNode: LocatedNode): undefined | LocatedNode => {
    let parent = undefined;
    if (locatedNode && locatedNode.context.parentNodes.length) {
      const context = locatedNode.context;
      const parentNode = context.parentNodes.length ? context.parentNodes[context.parentNodes.length - 1] : undefined;
      const parentNodes = [];
      for (let i = 0; i < context.parentNodes.length - 2; i++) {
        parentNodes.push();
      }
    }
    return parent;
  };
  
  locateHeading = (withinNode: AdfNode, level?: number): undefined | LocatedNode => {
    let locatedNode: undefined | LocatedNode = undefined;
    this._iterateAdf(withinNode, (adfNode: AdfNode, context: IterationContext) => {
      if (adfNode.type === 'heading') {
        if (!level || adfNode.attrs && adfNode.attrs.level === level) {
          locatedNode = {
            node: adfNode,
            context: this._iterationContextToAdfContext(context)
          };
          // const parentLocation = this.getParentLocation(locatedNode);
          // console.log('Found heading:', adfNode);
          // console.log('Found heading section:', JSON.stringify(context.parentNodes[context.parentNodes.length - 1], null, 2));
        }
        if (locatedNode) {
          return false;
        }
      }
    });
    return locatedNode;
  };

  getTextLogicallyWithinHeading = (withinNode: AdfNode, headingText: string, level?: number): undefined | string => {
    let text = undefined;
    const nodes = this.getNodesLogicallyWithinHeading(withinNode, headingText, level);
    if (nodes) {
      text = this.getConcatenatedTextOfNodes(nodes);
    }
    return text;
  };

  countLinksLogicallyWithinHeading = (withinNode: AdfNode, headingText: string, level?: number): number => {
    let count = 0;
    const nodes = this.getNodesLogicallyWithinHeading(withinNode, headingText, level);
    if (nodes) {
      for (const node of nodes) {
        count += this.countLinksInNode(node);
      }
    }
    return count;
  };

  countLinksInNode = (withinNode: AdfNode): number => {
    const linkNodes = this._findNodes(withinNode, 'link');
    const cardNodes = this._findNodes(withinNode, 'inlineCard');
    return linkNodes.length + cardNodes.length;
  };

  getNodesLogicallyWithinHeading = (withinNode: AdfNode, headingText: string, level?: number): AdfNode[] => {
    const nodes: AdfNode[] = [];
    let headingNode: undefined | AdfNode = undefined;
    let headingNodeLevel: undefined | number = undefined;
    let subsequentSignificantHeading: undefined | AdfNode = undefined;
    this._iterateAdf(withinNode, (adfNode: AdfNode, context: IterationContext) => {
      if (subsequentSignificantHeading) {
        return false;
      }
      if (headingNodeLevel !== undefined && context.level > headingNodeLevel) {
        // skip iteration into nested nodes
        return true;
      }
      let appendThisNode = headingNode !== undefined;
      if (adfNode.type === 'heading') {
        if (headingNode && !subsequentSignificantHeading) {
          if (adfNode.attrs.level >= headingNode.attrs.level) {
            subsequentSignificantHeading = adfNode;
            // console.log('Found subsequent significant heading node:', JSON.stringify(subsequentSignificantHeading, null, 2));
            appendThisNode = false;
          } else {
            //appendThisNode = true;
          }
        } else {
          const requiredLevel = !level || adfNode.attrs.level === level;
          if (requiredLevel) {
            const text = this.getConcatenatedSectionText(adfNode);
            if (text === headingText) {
              // console.log('Found heading node:', headingText, ':', JSON.stringify(adfNode, null, 2));
              headingNode = adfNode;
              headingNodeLevel = context.level;
            } else {
              //appendThisNode = true;
            }
          } else {
            //appendThisNode = true;
          }
        }
      } else {
        // This node is not a heading
        //appendThisNode = true;
      }
      if (appendThisNode) {
        // console.log('Adding node:', JSON.stringify(adfNode, null, 2));
        nodes.push(adfNode);
      }
    });
    return nodes;
  };

  getTableCellPosition = (tableAdfNode: AdfNode, tableCellNode: AdfNode): undefined | TableCellPosition => {
    pbc.assertTruthy(tableAdfNode, 'tableAdfNode');
    for (let rowIndex = 0; rowIndex < tableAdfNode.content.length; rowIndex++) {
      const rowNode = tableAdfNode.content[rowIndex];
      for (let columnIndex = 0; columnIndex < rowNode.content.length; columnIndex++) {
        const cellNode = rowNode.content[columnIndex];
        if (cellNode === tableCellNode) {
          return {
            columnIndex: columnIndex,
            rowIndex: rowIndex
          }
        }
      }
    }
    return undefined;
  };

  getCell = (tableAdfNode: AdfNode, position: TableCellPosition) => {
    pbc.assertTruthy(tableAdfNode, 'tableAdfNode');
    const row = tableAdfNode.content[position.rowIndex];
    const cell = row.content[position.columnIndex];
    return cell;
  };

  findTableHeadersWithText = (tableAdfNode: AdfNode, text: string): AdfNode[] => {
    return this._findNodes(tableAdfNode, 'tableHeader', text);
  };

  _findNodes = (tableAdfNode: AdfNode, nodeType: string, text?: string): AdfNode[] => {
    let nodes: AdfNode[] = [];
    this._iterateAdf(tableAdfNode, (adfNode: AdfNode, context: IterationContext) => {
      if (adfNode.type === nodeType) {
        if (text) {
          const contentFound = this.hasAllContent(adfNode, [text]);
          if (contentFound) {
            nodes.push(adfNode);
          }
        } else {
          nodes.push(adfNode);
        }
      }
    });
    return nodes;
  };

  findTableCell = (tableAdfNode: AdfNode, columnIndex: number, rowIndex: number): AdfNode => {
    const tableRow = this.findTableRow(tableAdfNode, rowIndex);
    const tableCell = this.findTableCellWithinRow(tableRow, columnIndex);
    return tableCell;
  };

  findTableRow = (tableAdfNode: AdfNode, rowIndex: number): AdfNode => {
    const tableRowContentItems = tableAdfNode.content;
    const tableRow = tableRowContentItems[rowIndex];
    return tableRow;
  };

  findTableCellWithinRow = (tableRowAdfNode: AdfNode, columnIndex: number): AdfNode => {
    const tableCellContentItems = tableRowAdfNode.content;
    const tableCell = tableCellContentItems[columnIndex];
    return tableCell;
  };

  countTableRows = (tableAdfNode: AdfNode) => {
    return tableAdfNode.content.length;
  };

  countTableColumns = (tableAdfNode: AdfNode) => {
    const tableRowContentItems = tableAdfNode.content;
    const tableRow = tableRowContentItems[0];
    return tableRow.content.length;
  };

  findTable = (adfNode: AdfNode, containingContentStrings): AdfNode => {
    let foundTableAdfNode = undefined;
    this._iterateAdf(adfNode, (adfNode: AdfNode, context: IterationContext) => {
      if (adfNode.type === 'table') {
        const contentFound = this.hasAllContent(adfNode, containingContentStrings);
        if (contentFound) {
          foundTableAdfNode = adfNode;
        }
      }
    });
    return foundTableAdfNode;
  };

  findFirstTableInNodes = (adfNodes: AdfNode[]): undefined | AdfNode => {
    for (const adfNode of adfNodes) {
      const tableNode = this.findTable(adfNode, []);
      if (tableNode) {
        return tableNode;
      }
    }
    return undefined;
  };

  findTableRowByHeaderColumnText = (tableAdfNode: AdfNode, containingContentStrings: string[]): undefined | AdfNode => {
    const tableRowContentItems = tableAdfNode.content;
    for (const tableRowContentItem of tableRowContentItems) {
      if (tableRowContentItem.content && tableRowContentItem.content.length) {
        for (const tableCellContentItem of tableRowContentItem.content) {
          // const cellText = this.getConcatenatedSectionText(tableCellContentItem);
          const contentFound = this.hasAllContent(tableCellContentItem, containingContentStrings);
          if (contentFound) {
            return tableRowContentItem;
          }
        }
      }
    }
    return undefined;
  };

  iterateTableRows = (tableNode: AdfNode, callback: Function) => {
    // console.log('Table has ' + tableNode.content.length + ' rows to iterate...');
    for (let rowIndex = 0; rowIndex < tableNode.content.length; rowIndex++) {
      const rowNode = tableNode.content[rowIndex];
      callback(rowNode, rowIndex);
    }
  };

  iterateRowCells = (rowNode: AdfNode, callback: Function) => {
    // console.log('Row has ' + rowNode.content.length + ' cells to iterate...');
    for (let columnIndex = 0; columnIndex < rowNode.content.length; columnIndex++) {
      const cellNode = rowNode.content[columnIndex];
      callback(cellNode, columnIndex);
    }
  };

  getConcatenatedTextOfNodes = (adfNodes: AdfNode[]): string => {
    let text = '';
    let nextSeparator = '';
    for (const adfNode of adfNodes) {
      text += nextSeparator + this.getConcatenatedSectionText(adfNode);
      nextSeparator = ' ';
    }
    return text;
  };

  getConcatenatedSectionText = (adfNode: AdfNode): string => {
    const texts = this.getSectionTexts(adfNode);
    return texts.join(' ');
  };

  getSectionTexts = (adfNode: AdfNode): string[] => {
    pbc.assertTruthy(adfNode, 'adfNode');
    const texts: string[] = [];
    this._iterateAdf(adfNode, (adfNode: AdfNode, context: IterationContext) => {
      if (adfNode.type === 'text') {
        texts.push(adfNode.text);
      }
    });
    return texts;
  };

  getStatusColor = (statusNode: AdfNode): string => {
    pbc.assertTruthy(statusNode, 'statusNode');
    pbc.assertEqual(statusNode.type, 'status', 'statusNode');
    pbc.assertTruthy(statusNode.attrs, 'statusNode.attrs');
    return statusNode.attrs.color;
  };

  getStatusText = (statusNode: AdfNode): string => {
    pbc.assertTruthy(statusNode, 'statusNode');
    pbc.assertEqual(statusNode.type, 'status', 'statusNode');
    pbc.assertTruthy(statusNode.attrs, 'statusNode.attrs');
    return statusNode.attrs.text;
  };

  getDate = (dateNode: AdfNode): Date => {
    pbc.assertTruthy(dateNode, 'dateNode');
    pbc.assertEqual(dateNode.type, 'date', 'dateNode.type');
    pbc.assertTruthy(dateNode.attrs, 'dateNode.attrs');
    return dateNode.attrs.timestamp ? new Date(parseInt(dateNode.attrs.timestamp, 10)) : undefined;
  };

  getMentionNames = (mentionNodes: AdfNode[]): string => {
    let text = '';
    let nextSeparator = '';
    for (const mentionNode of mentionNodes) {
      text += nextSeparator + this.getMentionName(mentionNode);
      nextSeparator = ', ';
    }
    return text;
  };

  getMentionName = (mentionNode: AdfNode): string => {
    return this.getMentionText(mentionNode).substring(1);
  };

  getMentionText = (mentionNode: AdfNode): string => {
    pbc.assertTruthy(mentionNode, 'mentionNode');
    pbc.assertEqual(mentionNode.type, 'mention', 'mentionNode.type');
    pbc.assertTruthy(mentionNode.attrs, 'mentionNode.attrs');
    return mentionNode.attrs.text;
  };

  findStatusNodes = (adfNode: AdfNode): AdfNode[] => {
    return this._findNodesOfType(adfNode, 'status');
  };

  findMentionNodes = (adfNode: AdfNode): AdfNode[] => {
    return this._findNodesOfType(adfNode, 'mention');
  };

  findDateNodes = (adfNode: AdfNode): AdfNode[] => {
    return this._findNodesOfType(adfNode, 'date');
  };

  findEmojiNodes = (adfNode: AdfNode): AdfNode[] => {
    return this._findNodesOfType(adfNode, 'emoji');
  };

  _findNodesOfType = (adfNode: AdfNode, nodeType: string): AdfNode[] => {
    const nodes: AdfNode[] = [];
    this._iterateAdf(adfNode, (adfNode: AdfNode, context: IterationContext) => {
      if (adfNode.type === nodeType) {
        nodes.push(adfNode);
      }
    });
    return nodes;
  };

  hasAllContent = (adfNode: AdfNode, containingContentStrings: string[]) => {
    const matchInfos = Array.from(containingContentStrings, (text) => {
      return {
        text: text,
        found: false
      }
    });
    this._iterateAdf(adfNode, (adfNode: AdfNode, context: IterationContext) => {
      if (adfNode.type === 'text') {
        const text = adfNode.text;
        matchInfos.map((matchInfo) => {
          if (!matchInfo.found) {
            if (text.indexOf(matchInfo.text) >= 0) {
              matchInfo.found = true;
            }
          }
        });
      }
    });
    for (const matchInfo of matchInfos) {
      if (!matchInfo.found) {
        return false;
      }
    }
    return true;
  };

  _iterateAdf = (adfNode: AdfNode, callback) => {
    const adfContext = {
      level: 0,
      continueIterating: true,
      parentNodes: []
    };
    this._iterateAdfWithContext(adfNode, adfContext, callback);
  };

  _iterateAdfWithContext = (adfNode: AdfNode, context: IterationContext, callback) => {
    if (!adfNode) {
      return;
    }
    const continueIterating = callback(adfNode, context);
    if (continueIterating === false) {
      context.continueIterating = false;
    }
    if (adfNode.content && adfNode.content.length) {
      //console.log('Pushing:', adfNode);
      context.level++;
      context.parentNodes.push(adfNode);
      for (const contentItem of adfNode.content) {
        this._iterateAdfWithContext(contentItem, context, callback);
        if (!context.continueIterating) {
          break;
        }
      }
      context.level--;
      context.parentNodes.pop();
    }
    if (adfNode.marks && adfNode.marks.length) {
      //console.log('Pushing:', adfNode);
      context.level++;
      context.parentNodes.push(adfNode);
      for (const contentItem of adfNode.marks) {
        this._iterateAdfWithContext(contentItem, context, callback);
        if (!context.continueIterating) {
          break;
        }
      }
      context.level--;
      context.parentNodes.pop();
    }
  };

  _iterationContextToAdfContext = (context: IterationContext): AdfContext => {
    const adfContext: AdfContext = {
      parentNodes: []
    };
    for (const node of context.parentNodes) {
      adfContext.parentNodes.push(node);
    }
    return adfContext;
  };

}

export default new AdfUtil()
